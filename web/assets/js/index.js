const apiHost = 'http://localhost:4000'; // used for docker
// const apiHost = 'http://bark-recruit-api.local'; // used for vagrant
$(document).ready(function(){
    $('.validation-error').hide();


    $('.js-autocomplete-services').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/services`
        }
    });

    $('.js-autocomplete-location').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/locations`
        }
    });

    $('.js-submit-lead').submit(function(e) {
        e.preventDefault();
        $('.validation-error').hide();
        var data = $(this).serialize();
        $(this).trigger('reset');

        $.post(
            `${apiHost}/api/leads`,
            data,
            function( data ) {
                $('#new-lead-success').modal('show');
            }
        ).fail((response) => {
           if (response.status === 422) {
               $('.validation-error').show();
           }

           if (response.status === 500) {
               alert("server error")
           }
        });
        return false;
    });

});
