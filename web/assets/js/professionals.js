const apiHost = 'http://localhost:4000'; // used for docker
// const apiHost = 'http://bark-recruit-api.local'; // used for vagrant

// onload
$(document).ready(function(){

    $('.js-autocomplete-services').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/services`
        }
    });

    $('.js-autocomplete-location').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/locations`
        }
    });

    (new LeadsList).init()

});

class LeadsList {

    init() {
        this.loadList();
        this.initDomItems();
        this.templateListItem = Handlebars.compile($('#handlebars-template-list-item').html());
        this.templateListDetail = Handlebars.compile($('#handlebars-template-list-detail').html());
        this.initListeners();
    }

    initDomItems() {
        this.leadListDom = $('#js-lead-list');
        this.leadDetailDom = $('#js-lead-details');
        this.leadDetailContainerDom = $('#js-lead-details-container');
        this.filterServiceDom = $('input[name=service_id]');
        this.filterLocationDom = $('input[name=location_id]');
        this.searchButtonDom = $('#js-search-button');
        this.loadMoreDom = $('#js-load-more');
    }

    initListeners() {
        this.leadListDom.on(
            'click', '.js-list-item',
            (event) => this.loadLeadDetail($(event.currentTarget).data('leadId'))
        );
        this.searchButtonDom.click((event) => {
            event.preventDefault();
            let serviceId = this.filterServiceDom.val()
            let locationId = this.filterLocationDom.val()
            this.loadList(serviceId, locationId)
        });
        this.loadMoreDom.on('click', (event) => {
            let serviceId = this.filterServiceDom.val()
            let locationId = this.filterLocationDom.val()
            let existingLink = $(event.currentTarget).attr('data-next-link')
            this.loadList(serviceId, locationId, existingLink)
        })
    }

    loadLeadDetail(id) {
        this.getLeadFromApi(id)
            .then(response => response.json())
            .then(lead => this.renderLead(lead));
    }

    loadList(serviceId, locationId, existingUrl) {
        this.getListFromApi(serviceId, locationId, existingUrl)
            .then(response => response.json())
            .then(leadList => this.renderList(leadList));
    }

    renderLead(lead) {
        this.leadDetailContainerDom.removeClass('d-none');
        this.leadDetailDom.html(this.templateListDetail(lead));
    }

    renderList(leadList) {
        if (leadList.current_page === 1) {
            this.leadListDom.html('');
        }
        if (leadList.next_page_url) {
            this.loadMoreDom.attr('data-next-link', leadList.next_page_url)
        } else {
            this.loadMoreDom.hide()
        }

        for (let lead of leadList.data) {
            if (this.displayLead(lead)) {
                this.leadListDom.append(this.templateListItem(lead))
            }
        }
    }

    displayLead(lead) {
        let serviceIdFilter = parseInt(this.filterServiceDom.val());
        let locationIdFilter = parseInt(this.filterLocationDom.val());

        if (serviceIdFilter && lead.service_id !== serviceIdFilter) {
            return false;
        }
        if (locationIdFilter && lead.location_id !== locationIdFilter) {
            return false;
        }

        return true;
    }

    getListFromApi(serviceId, locationId, existingUrl) {
        if (existingUrl) {
            return fetch(
                existingUrl,
                {
                    method: 'GET'
                }
            )
        } else {
            let queryString = serviceId ? `?service_id=${serviceId}` : ''
            queryString += !serviceId ? '?': '&';
            queryString += locationId ? `location_id=${locationId}` : ''
            return fetch(
                `${apiHost}/api/leads${queryString}`,
                {
                    method: 'GET'
                }
            )
        }

    }

    getLeadFromApi(id) {
        return fetch(
            `${apiHost}/api/leads/${id}`,
            {
                method: 'GET'
            }
        )
    }

}
