<?php

namespace Tests\Unit;

use App\Exceptions\CouldNotCreateLeadException;
use App\Repository\Lead\LeadRepository;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LeadRepositoryTest extends TestCase
{
    public function testLeadCreateSuccess(): void
    {
        $repository = new LeadRepository();

        $request = $this->createMock(Request::class);
        $request->expects(self::at(0))
            ->method('get')
            ->with('name')
            ->willReturn('Test');

        $request->expects(self::at(1))
            ->method('get')
            ->with('email')
            ->willReturn('example@example.com');

        $request->expects(self::at(2))
            ->method('get')
            ->with('phone')
            ->willReturn('07865643201');
        $request->expects(self::at(3))
            ->method('get')
            ->with('extra')
            ->willReturn('Test Description');
        $request->expects(self::at(4))
            ->method('get')
            ->with('location_id')
            ->willReturn(1);
        $request->expects(self::at(5))
            ->method('get')
            ->with('service_id')
            ->willReturn(1);

        $repository->create($request);
    }

    public function testCreateFailsWithException(): void
    {
        $this->expectException(CouldNotCreateLeadException::class);
        $repository = new LeadRepository();

        $request = $this->createMock(Request::class);
        $request->expects(self::at(0))
            ->method('get')
            ->with('name')
            ->willReturn('Test');

        $repository->create($request);
    }
}