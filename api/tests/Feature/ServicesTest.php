<?php
declare(strict_types=1);

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class ServicesTest extends  TestCase
{
    public function testServicesFetch(): void
    {
        $response = $this->get('/api/services');
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testServicesQuery(): void
    {
        $query = 'House';
        $url = sprintf('/api/services?q=%s', $query);
        $response = $this->get($url);
        $response->assertStatus(Response::HTTP_OK);
        $content = $response->getContent();
        $decodedContents = json_decode($content, true);
        self::assertCount(2, $decodedContents);
        foreach ($decodedContents as $service) {
            self::assertContains($query, $service['text']);
        }
    }
}