<?php
declare(strict_types=1);

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class LeadTest extends  TestCase
{
    public function testLeadFetch(): void
    {
        $response = $this->get('/api/leads');
        $response->assertStatus(Response::HTTP_OK);
        $content = $response->getContent();
        self::assertJson($content);
    }

    public function testLeadFetchById(): void
    {
        $id = 1;
        $url = sprintf('/api/leads/%d', $id);
        $response = $this->get($url);
        $response->assertStatus(Response::HTTP_OK);
        $content = $response->getContent();
        self::assertJson($content);
    }

    public function testLeadCreation(): void
    {
        $data = [
            'name' => 'Test',
            'email' => 'example@example.com',
            'extra' => 'include the description',
            'phone' => 07776564320,
            'location_id' => 1,
            'service_id' => 1,
        ];

        $response = $this->post('/api/leads', $data);
        $response->assertStatus(Response::HTTP_CREATED);
        $content = $response->getContent();
        self::assertSame('{"status":true}', $content);
    }

    public function testLeadCreationHandlesValidationWithoutPhone(): void
    {
        $data = [
            'name' => 'Test',
            'email' => 'example@example.com',
            'extra' => 'include the description',
            'location_id' => 1,
            'service_id' => 1,
        ];

        $response = $this->post('/api/leads', $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}