<?php

namespace App\Providers;

use App\Repository\Lead\LeadRepository;
use App\Repository\Lead\LeadRepositoryInterface;
use App\Repository\Location\LocationRepository;
use App\Repository\Location\LocationRepositoryInterface;
use App\Repository\Service\ServiceRepository;
use App\Repository\Service\ServiceRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(LeadRepositoryInterface::class, LeadRepository::class);
        $this->app->bind(LocationRepositoryInterface::class, LocationRepository::class);
        $this->app->bind(ServiceRepositoryInterface::class, ServiceRepository::class);
    }
}
