<?php
declare(strict_types=1);

namespace App\Repository\Lead;

use App\Exceptions\CouldNotCreateLeadException;
use App\Exceptions\CouldNotFetchLeadException;
use App\Models\Lead;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class LeadRepository implements LeadRepositoryInterface
{
    /**
     * @param Request $request
     * @throws CouldNotCreateLeadException
     */
    public function create(Request $request): void
    {
       $input = [
           'name' => $request->get('name'),
           'email' => $request->get('email'),
           'phone' => $request->get('phone'),
           'extra' => $request->get('extra'),
           'location_id' => $request->get('location_id'),
           'service_id' => $request->get('service_id'),
       ];

       try {
           Lead::create($input);
       } catch (QueryException $exception) {
           // do logging
           throw new CouldNotCreateLeadException("There was an error creating the lead");
       }
    }

    /**
     * @param Request $request
     * @return Paginator
     */
    public function fetch(Request $request): Paginator
    {
        $serviceId = $request->get('service_id');
        $locationId = $request->get('location_id');

        $query = Lead::query()->with(['location', 'service']);
        if ($serviceId) {
            $query->where('service_id', $serviceId);
        }

        if ($locationId) {
            $query->where('location_id', $locationId);
        }

        $leads = $query->paginate(10);
        $leads->appends('service_id', $serviceId);
        $leads->appends('location_id', $locationId);

        return $leads;
    }

    /**
     * @param string $id
     * @return Lead
     * @throws CouldNotFetchLeadException
     */
    public function fetchById(string $id): Lead
    {
        try {
            return Lead::with(['location', 'service'])->find($id);
        } catch (QueryException $exception) {
            throw new CouldNotFetchLeadException("Could Not fetch Lead");
        }
    }
}