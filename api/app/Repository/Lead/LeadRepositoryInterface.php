<?php
declare(strict_types=1);

namespace App\Repository\Lead;


use App\Exceptions\CouldNotCreateLeadException;
use App\Exceptions\CouldNotFetchLeadException;
use App\Models\Lead;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;

interface LeadRepositoryInterface
{
    /**
     * @param Request $request
     * @throws CouldNotCreateLeadException
     */
    public function create(Request $request): void;

    public function fetch(Request $request): Paginator;

    /**
     * @param string $id
     * @return Lead
     * @throws CouldNotFetchLeadException
     */
    public function fetchById(string $id): Lead;
}