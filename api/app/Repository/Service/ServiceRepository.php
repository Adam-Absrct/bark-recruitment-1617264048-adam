<?php
declare(strict_types=1);

namespace App\Repository\Service;

use App\Exceptions\CouldNotFetchServicesException;
use App\Models\Service;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ServiceRepository implements ServiceRepositoryInterface
{

    /**
     * @param Request $request
     * @return array
     * @throws CouldNotFetchServicesException
     */
    public function listServices(Request $request): array
    {
        $query = $request->get('q');
        try {
            if ($query) {
                $services = Service::whereRaw("name like '$query%'")->get();
            } else {
                $services = Service::all();
            }
        } catch (QueryException $exception) {
            throw new CouldNotFetchServicesException("There was an error fetching the services.");
        }

        $servicesFormatted = [];
        foreach ($services as $service) {
            $servicesFormatted[] = [
                'value' => $service->id,
                'text' => $service->name,
            ];
        }

        return $servicesFormatted;
    }
}