<?php
declare(strict_types=1);

namespace App\Repository\Service;

use App\Exceptions\CouldNotFetchServicesException;
use Illuminate\Http\Request;

interface ServiceRepositoryInterface
{
    /**
     * @param Request $request
     * @return array
     * @throws CouldNotFetchServicesException
     */
    public function listServices(Request $request): array;
}