<?php
declare(strict_types=1);

namespace App\Repository\Location;

use App\Exceptions\CouldNotFetchLocationException;
use Illuminate\Http\Request;

interface LocationRepositoryInterface
{
    /**
     * @param Request $request
     * @return array
     * @throws CouldNotFetchLocationException
     */
    public function listLocation(Request $request): array;
}
