<?php
declare(strict_types=1);

namespace App\Repository\Location;

use App\Exceptions\CouldNotFetchLocationException;
use App\Models\Location;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class LocationRepository implements LocationRepositoryInterface
{

    /**
     * @param Request $request
     * @return array
     * @throws CouldNotFetchLocationException
     */
    public function listLocation(Request $request): array
    {
        $query = $request->get('q');
        try {
            if ($query) {
                $locations = Location::whereRaw("name like '$query%'")->get();
            } else {
                $locations = Location::all();
            }
        } catch (QueryException $exception) {
            throw new CouldNotFetchLocationException("There was an error trying to retrieve location");
        }


        $locationsFormatted = [];
        foreach ($locations as $location) {
            $locationsFormatted[] = [
                'value' => $location->id,
                'text' => $location->name,
            ];
        }

        return $locationsFormatted;
    }
}

