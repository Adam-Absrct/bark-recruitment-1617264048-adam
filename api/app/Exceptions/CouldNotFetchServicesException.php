<?php

namespace App\Exceptions;

use Exception;
use Log;

class CouldNotFetchServicesException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        Log::debug('Could Not Fetch Services');
    }
}