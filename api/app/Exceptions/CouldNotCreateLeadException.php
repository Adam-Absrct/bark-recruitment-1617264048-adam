<?php

namespace App\Exceptions;

use Exception;
use Log;

class CouldNotCreateLeadException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        Log::debug('Could Not Create Lead');
    }
}