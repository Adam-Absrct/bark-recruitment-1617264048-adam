<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Lead
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $slug
 */
class Lead extends Model
{
    protected $table = 'leads';

    protected $fillable = [
        'name',
        'email',
        'phone',
        'more_info',
        'location_id',
        'service_id'
    ];


    public function getNameAttribute($value)
    {
        $name= explode(' ', $value);
        return $name[0];
    }

    public function getPhoneAttribute($value)
    {
        return substr_replace($value, '****', -4);
    }

    public function getEmailAttribute($value)
    {
        $pattern = '/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/';
        return preg_replace($pattern, '*', $value);
    }

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

}