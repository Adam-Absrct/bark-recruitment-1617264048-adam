<?php namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Exceptions\CouldNotFetchLocationException;
use App\Repository\Location\LocationRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class LocationsController extends Controller
{
    /**
     * @var LocationRepositoryInterface
     */
    private $repository;

    public function __construct(LocationRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    /**
     * Get locations with a possible query param to search
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        try {
            $locationsFormatted = $this->repository->listLocation($request);
            return response()->json($locationsFormatted, Response::HTTP_OK);
        } catch (CouldNotFetchLocationException $exception) {
            return response()->json([
                'status'    =>  false,
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}



