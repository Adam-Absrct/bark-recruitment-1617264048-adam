<?php namespace App\Http\Controllers;

namespace App\Http\Controllers;

use App\Exceptions\CouldNotFetchServicesException;
use App\Models\Service;
use App\Repository\Service\ServiceRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;


class ServicesController extends Controller
{
    /**
     * @var ServiceRepositoryInterface
     */
    private $repository;

    public function __construct(ServiceRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    /**
     * Get services with a possible query param to search
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        try {
            $servicesFormatted = $this->repository->listServices($request);
            return response()->json($servicesFormatted, Response::HTTP_OK);
        } catch (CouldNotFetchServicesException $exception) {
            return response()->json([
                'status'    =>  false,
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}



