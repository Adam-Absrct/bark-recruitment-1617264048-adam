<?php namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Exceptions\CouldNotCreateLeadException;
use App\Repository\Lead\LeadRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;


class LeadsController extends Controller
{

    /**
     * @var LeadRepositoryInterface
     */
    private $repository;

    public function __construct(LeadRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Find the nearest city based on the provided lat / lng
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:200',
            'email' => 'required|max:200',
            'phone' => 'required|max:100',
            'extra' => 'max:1000',
            'location_id' => 'required|integer',
            'service_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
           $this->repository->create($request);
            return response()->json([
                'status'    =>  true
            ], Response::HTTP_CREATED);
        } catch (CouldNotCreateLeadException $exception) {
            return response()->json([
                'status'    =>  false,
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function all(Request $request)
    {
        try {
            return $this->repository->fetch($request);
        } catch (\Exception $exception) {
            return response()->json([
                'status'    =>  false,
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function one(string $id)
    {
        try {
            return $this->repository->fetchById($id);
        } catch (\Exception $exception) {
            return response()->json([
                'status'    =>  false,
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
